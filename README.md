This plugin has been forked and renamed [roz-maven-plugin](https://bitbucket.org/atlassian/roz-maven-plugin).

The last release under the `checkdeps-maven-plugin` name was `1.0.2` at [this commit](https://bitbucket.org/atlassian/roz-maven-plugin/commits/93dc0b68118155bdbb122c0cb6707caeac906f3d).